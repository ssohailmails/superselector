﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.RecyclerView.Extensions;
using Android.Views;
using Android.Widget;

namespace SuperSelector
{
    [Activity(Label = "SelectedPlayer")]
    public class SelectedPlayer : ListActivity
    {
         string[] itemsTwo;
        public static string selectedProfile;

        public string[] selectedCandids { get; set; }
        public List<string> lsAgain;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);


             itemsTwo = MainActivity.ls.ToArray();


            base.ListAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1,itemsTwo);


        }


        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            selectedProfile = itemsTwo[position];

            StartActivity(typeof(Profile));


        }


    }

    
}