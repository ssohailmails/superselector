﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace SuperSelector
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : ListActivity
    {
       public static string[] items;
        string[] itemsSelected;
        public static List<string> ls;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            itemsSelected = new string[6];
            ls = new List<string>();

            items = new string[] { "Shikar Dhawan", "Tendulkar", "Rahul Dravid", "Mohammed Kaif", "Yuvraj Singh", "Ms Dhoni","Hardik pandya"};
            ListAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, items);


        }

        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var t = items[position];
            if (ls.Count <= 3)
            {
                ls.Add(t);

            }
            else
            {
                System.Diagnostics.Debug.WriteLine(ls);
                Bundle b = new Bundle();
                b.PutStringArrayList("players", ls);
                Intent i = new Intent(this, typeof(SelectedPlayer));
                i.PutExtras(b);
                StartActivity(i);

            }
            // Android.Widget.Toast.MakeText(this, t, Android.Widget.ToastLength.Short).Show();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}

