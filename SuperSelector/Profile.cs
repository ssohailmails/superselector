﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SuperSelector
{
    [Activity(Label = "Profile")]
    public class Profile : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Profile);

            TextView textOne = FindViewById<TextView>(Resource.Id.nameID);
            textOne.Text = SelectedPlayer.selectedProfile.ToString();
            string name = SelectedPlayer.selectedProfile.ToString();

            TextView textTwo = FindViewById<TextView>(Resource.Id.ageID);
            textTwo.Text = "30 years";

            ImageView playerImage = FindViewById<ImageView>(Resource.Id.profilePic);

            if ( name == "Shikar Dhawan") {
            }else if (name == "Tendulkar")
            {
                playerImage.SetImageResource(Resource.Drawable.sachin);


            }
            else if (name == "Rahul Dravid")
            {
                playerImage.SetImageResource(Resource.Drawable.dravid);

            }
            else if (name == "Mohammed Kaif")
            {
                playerImage.SetImageResource(Resource.Drawable.kaif);

            }
            else if (name == "Yuvraj Singh")
            {
                playerImage.SetImageResource(Resource.Drawable.yuvraj);

            }
            else if (name == "Ms Dhoni")
            {
                playerImage.SetImageResource(Resource.Drawable.dhoni);

            }
            else if (name == "Hardik pandya")
            {
                playerImage.SetImageResource(Resource.Drawable.hardik);

            }


            // Create your application here
        }
    }
}
